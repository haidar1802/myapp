import React from 'react';
import Navbar from './components/Navbar';
import Sidebar from './components/Sidebar';
// import ApexCharts from 'react-apexcharts';
const app = () => {
  

  return (
    <>
      <Navbar/>
      <Sidebar />

    </>
  );
};

export default app;
