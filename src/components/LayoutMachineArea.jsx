import React from 'react'
import card1 from "../assets/card1.svg";
import lingkaranpersen from "../assets/lingkaranpersen.svg"
import elipsred from "../assets/elipsred.svg"

const LayoutMachineArea = () => {
    return (
        <div className="row " style={{ width: "auto" }}>
            <div className="card" >
                <div className="row py-2" style={{ backgroundColor: "#151618" }}>
                    <div className="col-lg-12" >
                        <h6 className='text-center text-white' style={{ fontSize: 9 }}>Layout Machine Area</h6>
                    </div>
                </div>

                <div className="row text-white py-1" style={{ backgroundColor: "#151618" }}>
                    <div className="row mx-auto">
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2 " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                    </div>

                    <div className="row mx-auto my-1">
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2 " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                    </div>

                    <div className="row mx-auto">
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2 " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col  mx-2" style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                        <div className=" col " style={{ width: 33, height: 33, backgroundColor: "#33A02C" }}></div>
                    </div>

                    <div className="row mt-1 text-white">

                                <div className="col-lg-8" style={{ fontSize: 7 }}>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Stop</span>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Idle</span>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Running</span>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Disconnect</span>

                                </div>
                                <div className="col-lg-4" style={{ fontSize: 7 }}>
                                    <div className="d-flex flex-row-reverse">
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Alarm</span>
                                    
                                    </div>

                                </div>


                            </div>

                </div>




            </div>
        </div>
    )
}

export default LayoutMachineArea
