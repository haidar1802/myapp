import React from 'react'
import card1 from "../assets/card1.svg";
import lingkaranpersen from "../assets/lingkaranpersen.svg"
import elipsred from "../assets/elipsred.svg"
import persenblocksquare from "../assets/persenblocksquare.svg"
import topfrequensi from "../assets/topfrequensi.svg"


const TopFrequensi = () => {
    return (
        <div className="row " >
            <div className="card" >
                <div className="row py-2" style={{ backgroundColor: "#151618" }}>
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-12">
                                <h6 className="text-white" style={{ fontSize: 9 }}>5 Top Frequency Machine Alarm</h6>
                            </div>

                        </div>
                        <div >
                            <div className="row my-2">
                                <img src={topfrequensi} alt="" width={200} />
                            </div>
                        </div>



                    </div>

                </div>
            </div>
        </div>
    )
}

export default TopFrequensi
