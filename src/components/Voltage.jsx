import React from 'react'
import card1 from "../assets/card1.svg";
import lingkaranpersen from "../assets/lingkaranpersen.svg"
import elipsred from "../assets/elipsred.svg"
import persenblocksquare from "../assets/persenblocksquare.svg"
import grafik1 from "../assets/grafik1.svg"


const Voltage = () => {
    return (
        <div className="row " >
            <div className="card" >
                <div className="row pt-2" style={{ backgroundColor: "#151618" }}>
                    <div className="col-lg-12">
                        <div className="row">
                            <div className="col-lg-9">
                                <h6 className="text-white" style={{ fontSize: 9 }}>Voltage</h6>
                            </div>
                            <div className="col-lg-3 d-flex justify-content-space-between">

                            </div>

                        </div>
                        <div >
                            <div className="row ">
                                <img src={grafik1} alt="" width={200} />
                            </div>
                        </div>

                        <div className="row text-white ">
                            <div className="row mt-1 text-white">

                                <div className="col-lg-8" style={{ fontSize: 7 }}>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Minimum</span>
                                    <img src={elipsred} alt="" width={6} /><span className='px-1 me-2'>Actual</span>

                                </div>
                                <div className="col-lg-4" style={{ fontSize: 7 }}>
                                    <div className="d-flex flex-row-reverse">
                                         <img src={elipsred} alt="" width={6} />
                                        <img src={elipsred} alt="" width={6} className="mx-1"/>
                                        <img src={elipsred} alt="" width={6} />
                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    )
}

export default Voltage
