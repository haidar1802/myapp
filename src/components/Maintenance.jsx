import React from 'react'
import elipsred from "../assets/elipsred.svg";
import calender from "../assets/calendar.svg";
import ScheduleMaintenance from "../assets/schedulemaintenance.svg";


const Maintenance = () => {
    return (
        <div className="row " style={{ width: "auto" }}>
            <div className="card " style={{ backgroundColor: "#151618" }} >
                <div className="row py-2" style={{ backgroundColor: "#151618" }}>
                    <div className="col-lg-12" >
                        <h6 className='text-center text-white' style={{ fontSize: 9 }}>Maintenance</h6>
                    </div>
                </div>

                <div className="d-flex justify-content-center " >
                    <div className="row" >
                        <div className="col-lg-6 ">
                            <img src={calender} alt="" width={120} height={130} className="" />
                        </div>
                        <div className="col-lg-6 ">
                            <img src={ScheduleMaintenance} alt="" width={120} height={130} />
                        </div>
                    </div>

                </div>

            </div>
        </div>
    )
}

export default Maintenance
