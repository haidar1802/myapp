import React from 'react';

const MainContent = () => {
  // Sample data for demonstration
  const data = [
    { id: 1, title: 'Data 1', description: 'This is data 1.' },
    { id: 2, title: 'Data 2', description: 'This is data 2.' },
    { id: 3, title: 'Data 3', description: 'This is data 3.' },
    // Add more data as needed
  ];

  return (
    <div className='row'>
      {/* Render the data */}
      {data.map((item) => (
        <div key={item.id}>
          <h3>{item.title}</h3>
          <p>{item.description}</p>
        </div>
      ))}
    </div>
  );
};

export default MainContent;
