import React, { useState } from "react";
import { Col, Row } from "react-bootstrap";

const Calendar = ({ setDateNow }) => {
    // Mendapatkan tanggal saat ini
    const today = new Date();
    const currentMonth = today.getMonth();
    const currentYear = today.getFullYear();

    // State untuk menyimpan bulan dan tahun yang akan ditampilkan
    const [displayedMonth, setDisplayedMonth] = useState(currentMonth);
    const [displayedYear, setDisplayedYear] = useState(currentYear);
    //   const [prevMonth,setPrevMonth] = useState();

    // Fungsi untuk mendapatkan jumlah hari dalam bulan tertentu
    const daysInMonth = (month, year) => {
        return new Date(year, month + 1, 0).getDate();
    };

    // Mendapatkan tanggal awal dari bulan saat ini
    const startDay = new Date(displayedYear, displayedMonth, 1).getDay();

    // Mendapatkan jumlah hari dalam bulan saat ini
    const totalDays = daysInMonth(displayedMonth, displayedYear);

    // Membuat array yang berisi tanggal-tanggal dalam bulan saat ini
    const monthDates = Array.from({ length: totalDays }, (_, i) => i + 1);

    const changeMonth = (amount) => {
        let newMonth = displayedMonth + amount;
        let newYear = displayedYear;

        // Jika newMonth melebihi 11 (indeks bulan Desember), maka pindah ke tahun berikutnya
        // Jika newMonth kurang dari 0 (indeks bulan Januari), maka pindah ke tahun sebelumnya
        if (newMonth > 11) {
            newMonth = 0;
            newYear += 1;
        } else if (newMonth < 0) {
            newMonth = 11;
            newYear -= 1;
        }

        setDisplayedMonth(newMonth);
        setDisplayedYear(newYear);
    };

    return (
        <div className="container">
        <div className="row justify-content-center mt-5">
          <div className="col-lg-4">
            <div className="card">
              <div className="card-body">
                <h3 className="text-center mb-3">
                  {new Date(displayedYear, displayedMonth).toLocaleString(
                    "default",
                    {
                      month: "long",
                      year: "numeric",
                    }
                  )}
                </h3>
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th scope="col">Sun</th>
                      <th scope="col">Mon</th>
                      <th scope="col">Tue</th>
                      <th scope="col">Wed</th>
                      <th scope="col">Thu</th>
                      <th scope="col">Fri</th>
                      <th scope="col">Sat</th>
                    </tr>
                  </thead>
                  <tbody>
                    {/* ... rest of the code ... */}
                  </tbody>
                </table>
                <div className="text-center mt-3">
                  <button
                    className="btn btn-primary mx-2"
                    onClick={() => changeMonth(-1)}
                  >
                    Prev
                  </button>
                  <button
                    className="btn btn-primary mx-2"
                    onClick={() => changeMonth(1)}
                  >
                    Next
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
};

export default Calendar;
