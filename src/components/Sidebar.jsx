import React, { useState } from 'react';
// import "./css/file.css"
import ApexCharts from 'react-apexcharts';
import logo from "../assets/logo.svg";
import useSidebar from './helper/useSidebar';
const Sidebar = () => {
    const { handleDropdownClick, handleMouseLeave, handleMouseEnter, sidebarHidden } = useSidebar();

    return (
        <section id="sidebar" onMouseLeave={handleMouseLeave} onMouseEnter={handleMouseEnter}>
            <a href="#" className="brand d-flex justify-content-center">
                <img src={logo} alt="" height={30} />
            </a>
            <ul className="side-menu">
                <li>
                    <a href="#" className="active">
                        <i className="bx bxs-dashboard icon"></i> Dashboard
                    </a>
                </li>
                <li>
                    <a href="#" onClick={handleDropdownClick}>
                        <i className="bx bxs-inbox icon"></i> Data <i className="bx bx-chevron-right icon-right"></i>
                    </a>
                    <ul className="side-dropdown">
                        <li>
                            <a href="#">Alert</a>
                        </li>
                        <li>
                            <a href="#">Badges</a>
                        </li>
                        <li>
                            <a href="#">Breadcrumbs</a>
                        </li>
                        <li>
                            <a href="#">Button</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i className="bx bxs-chart icon"></i> Maintenance
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i className="bx bxs-widget icon"></i> Master Data
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i className="bx bxs-user icon"></i> Activity User
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i className="bx bxs-group icon"></i> Management User
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i className="bx bxs-group icon"></i> Account
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i className="bx bxs-group icon"></i> Setting
                    </a>
                </li>

                <li>
                    <a href="#">
                        <i className="bx bxs-group icon"></i> Management User
                    </a>
                </li>

                <li className='mt-4'>
                    <a href="#">
                        <i className="bx bx-table icon"></i> Log out
                    </a>
                </li>

            </ul>
            
        </section>
    );
};

export default Sidebar;
