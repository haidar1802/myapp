import React from 'react'
import card1 from "../assets/card1.svg";
import lingkaranpersen from "../assets/lingkaranpersen.svg"
import elipsred from "../assets/elipsred.svg"


const TimeMachine = () => {
    return (
        <div className="row bg-dark" >
            <div className="card" >
                <div className="row py-2" style={{ backgroundColor: "#151618" }}>
                    <div className="col-lg-8">
                        <div className="row">
                            <div className="col-lg-9">
                                <h6 className="text-white" style={{ fontSize: 9 }}>Time Machine Detail</h6>
                            </div>
                            <div className="col-lg-3 d-flex justify-content-space-between">

                            </div>

                        </div>
                        <div className="mt-2">
                            <div className="row ">
                                <div className="col-lg-7 mt-2">
                                    <img src={lingkaranpersen} alt="" width={100} />
                                </div>
                                <div className="col-lg-5 text-white mt-2">
                                    <div className="mt-4 mx-auto" style={{ fontSize: 8 }}>
                                        <img src={elipsred} alt="" width={5} /><span className='mx-1'>Stop Line</span> 
                                    </div>
                                    <div className="mt-2 mx-auto" style={{ fontSize: 8 }}>
                                        <img src={elipsred} alt="" width={5} /><span className='mx-1'>Idle</span> 
                                    </div>
                                    <div className="mt-2 mx-auto" style={{ fontSize: 8 }}>
                                        <img src={elipsred} alt="" width={5} /><span className='mx-1'>Running</span> 
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="col-lg-4">
                        <div className="row">
                            <h6 className="text-white bold" style={{ fontSize: 8 }}>MTTR | MTBF</h6>
                            <div className="col-lg-12">
                                <img src={card1} alt="MTTR | MTBF" style={{ height: 62, width: "auto" }} />
                                <img src={card1} alt="MTTR | MTBF" style={{ height: 62, width: "auto" }} className="mt-1" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TimeMachine
