import React, { useEffect, useState } from 'react';
import ApexCharts from 'react-apexcharts';
import Ampere from './Ampere';
import useSidebar from './helper/useSidebar'; //hooks custom
import LayoutMachineArea from './LayoutMachineArea';
import MachineActivity from './MachineActivity';
import Maintenance from './Maintenance';
import ProductionGraph from './ProductionGraph';
import TimeMachine from './TimeMachine';
import TopFrequensi from './TopFrequensi';
import Voltage from './Voltage';

const Navbar = () => {
  const { toggleSidebar } = useSidebar();

  return (
    <section id="content">
      {/* NAVBAR */}
      <nav style={{ backgroundColor: "#151618", display: "flex", alignItems: "center" }}>
        <div style={{ flexGrow: 1 }}>
          <i className="bx bx-menu toggle-sidebar me-2" onClick={toggleSidebar}></i>
          <span className='text-white' style={{ fontSize: 14 }}>Digitalization Line PRODUCTION</span>
        </div>
        <div className='text-white ms-3' style={{ fontSize: 14 }}>
          FRIDAY, 23 DECEMBER 2022 | 11:21:01
        </div>
        <a href="#" className="nav-link">
          <i className="bx bxs-bell icon"></i>
          <span className="badge">5</span>
        </a>

      </nav>
      {/* NAVBAR */}
      {/* MAIN */}
      <main className='bg-dark text-white'>
        <div className="container bg-dark">
          <div className="row bg-dark">
            <div className="col-lg-4">
              <TimeMachine />
              <MachineActivity />
              <Voltage />

            </div>
            <div className="col-lg-4">
              <LayoutMachineArea />
              <MachineActivity />
              <Ampere/>
              
            </div>
            <div className="col-lg-4">
              <Maintenance />
              <ProductionGraph/>
              <TopFrequensi />
              {/* <Maintenance /> */}
            </div>
          </div>
        </div>
        {/* <h1 className="title">Dashboard</h1>
        <ul className="breadcrumbs">
          <li>
            <a href="#">Home</a>
          </li>
          <li className="divider">/</li>
          <li>
            <a href="#" className="active">
              Dashboard
            </a>
          </li>
        </ul>
        <div className="info-data">
          <div className="card">
            <div className="head">
              <div>
                <h2>1500</h2>
                <p>Traffic</p>
              </div>
              <i className="bx bx-trending-up icon"></i>
            </div>
            <span className="progress" data-value="40%"></span>
            <span className="label">40%</span>
          </div>
          <div className="card">
            <div className="head">
              <div>
                <h2>234</h2>
                <p>Sales</p>
              </div>
              <i className="bx bx-trending-down icon down"></i>
            </div>
            <span className="progress" data-value="60%"></span>
            <span className="label">60%</span>
          </div>
          <div className="card">
            <div className="head">
              <div>
                <h2>465</h2>
                <p>Pageviews</p>
              </div>
              <i className="bx bx-trending-up icon"></i>
            </div>
            <span className="progress" data-value="30%"></span>
            <span className="label">30%</span>
          </div>
          <div className="card">
            <div className="head">
              <div>
                <h2>235</h2>
                <p>Visitors</p>
              </div>
              <i className="bx bx-trending-up icon"></i>
            </div>
            <span className="progress" data-value="80%"></span>
            <span className="label">80%</span>
          </div>
        </div>
        <div className="data">
          <div className="content-data">
            <div className="head">
              <h3>Sales Report</h3>
              <div className="menu">
                <i className="bx bx-dots-horizontal-rounded icon"></i>
                <ul className="menu-link">
                  <li>
                    <a href="#">Edit</a>
                  </li>
                  <li>
                    <a href="#">Save</a>
                  </li>
                  <li>
                    <a href="#">Remove</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="chart">
              <ApexCharts options={options} series={options.series} type="bar" height={options.chart.height} />
            </div>
          </div>
          <div className="content-data">
            <div className="head">
              <h3>Chatbox</h3>
              <div className="menu">
                <i className="bx bx-dots-horizontal-rounded icon"></i>
                <ul className="menu-link">
                  <li>
                    <a href="#">Edit</a>
                  </li>
                  <li>
                    <a href="#">Save</a>
                  </li>
                  <li>
                    <a href="#">Remove</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="chat-box">
              <p className="day">
                <span>Today</span>
              </p>
              <div className="msg">
                <img
                  src="https://images.unsplash.com/photo-1517841905240-472988babdf9?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cGVvcGxlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                  alt=""
                />
                <div className="chat">
                  <div className="profile">
                    <span className="username">Alan</span>
                    <span className="time">18:30</span>
                  </div>
                  <p>Hello</p>
                </div>
              </div>
              <div className="msg me">
                <div className="chat">
                  <div className="profile">
                    <span className="time">18:30</span>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque voluptatum eos quam dolores eligendi exercitationem animi nobis
                    reprehenderit laborum! Nulla.
                  </p>
                </div>
              </div>
              <div className="msg me">
                <div className="chat">
                  <div className="profile">
                    <span className="time">18:30</span>
                  </div>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsam, architecto!</p>
                </div>
              </div>
              <div className="msg me">
                <div className="chat">
                  <div className="profile">
                    <span className="time">18:30</span>
                  </div>
                  <p>Lorem ipsum, dolor sit amet.</p>
                </div>
              </div>
            </div>
            <form action="#">
              <div className="form-group">
                <input type="text" placeholder="Type..." />
                <button type="submit" className="btn-send">
                  <i className="bx bxs-send"></i>
                </button>
              </div>
            </form>
          </div>
        </div> */}
      </main>
      {/* MAIN */}
    </section>
  );
};

export default Navbar;
