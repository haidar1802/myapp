import React, { useState } from 'react';

const useSidebar = () => {
  const [sidebarHidden, setSidebarHidden] = useState(false);

  const handleDropdownClick = (e) => {
    e.preventDefault();
    const allDropdowns = document.querySelectorAll('#sidebar .side-dropdown');
    const allDropdownLinks = document.querySelectorAll('#sidebar .side-menu > li > a:first-child');

    if (!e.target.classList.contains('active')) {
      allDropdownLinks.forEach((link) => link.classList.remove('active'));
      allDropdowns.forEach((dropdown) => dropdown.classList.remove('show'));
    }

    e.target.classList.toggle('active');
    e.target.nextElementSibling.classList.toggle('show');
  };

  const handleMouseLeave = () => {
    if (sidebarHidden) {
      const allDropdownLinks = document.querySelectorAll('#sidebar .side-menu > li > a:first-child');
      const allSideDividers = document.querySelectorAll('#sidebar .divider');

      allDropdownLinks.forEach((link) => link.classList.remove('active'));
      allSideDividers.forEach((divider) => (divider.textContent = '-'));
    }
  };

  const handleMouseEnter = () => {
    if (sidebarHidden) {
      const allDropdownLinks = document.querySelectorAll('#sidebar .side-menu > li > a:first-child');
      const allSideDividers = document.querySelectorAll('#sidebar .divider');

      allDropdownLinks.forEach((link) => link.classList.remove('active'));
      allSideDividers.forEach((divider) => (divider.textContent = divider.dataset.text));
    }
  };

  const toggleSidebar = () => {
    setSidebarHidden(!sidebarHidden)
  };

  return { handleDropdownClick, handleMouseLeave, handleMouseEnter, sidebarHidden, toggleSidebar };
};

export default useSidebar;
